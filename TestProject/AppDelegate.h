//
//  AppDelegate.h
//  TestProject
//
//  Created by sanjay on 1/13/16.
//  Copyright © 2016 sanjay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

