//
//  ViewController.m
//  TestProject
//
//  Created by sanjay on 1/13/16.
//  Copyright © 2016 sanjay. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *catView = [[UIView alloc] initWithFrame:CGRectMake(10,10,320,400)];
    
    m_Image = [UIImage imageNamed:@"Standardbred_002.png"];
    
    CGDataProviderRef provider = CGImageGetDataProvider(m_Image.CGImage);
    NSData* data = (id)CFBridgingRelease(CGDataProviderCopyData(provider));
    const uint8_t* bytes = [data bytes];
    
    NSError *error = nil;
    NSString *path = @"/Users/Sanjay/Desktop/TestProject/Bytes.txt";
    [data writeToFile:path options:NSDataWritingAtomic error:&error];
    NSLog(@"Write returned error: %@", [error localizedDescription]);
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:m_Image];
    imageView.frame = catView.frame;
    
    [catView addSubview:imageView];
    
    [self.view addSubview:catView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
    
   
}

@end
